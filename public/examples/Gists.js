(function() {
  'use strict';

  angular.module('gists', [])
    .directive('someDirective', SomeDirective);


  function SomeDirective() {
    return {
      restrict: 'E',
      scope: {}, // or true, false
      bindToController: {}, // or true, false
      controller: function() {}, // The directive controller
      compile: function() {
        return {
          pre: function() {},
          post: function() {}
        };

        // could also return function, same as post link
      },
      link: function() {} // same as post-link
    };
  }
}());
