(function() {
  'use strict';

  angular.module('Airports')
    .directive('jsonTree', JSONTree);

  function JSONTree() {
    return {
      restrict: 'EA',
      tamplateUrl: 'jsonTree.html',
      scope: {
        data: '*='
      },
      controller: JSONTreeController,
      link: function(scope, el, attrs) {

      }
    };
  }

  // tableFromJSONDirectiveController.$inject = ['$scope'];

  function JSONTreeController($scope) {

  }

}());
