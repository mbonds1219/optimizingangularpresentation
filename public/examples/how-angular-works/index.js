(function() {
  'use strict';
  angular.module('HowItWorks', [])
    .run(howItWorks)
    .directive('digestCycleCount', DigestCycleCountDirective)
    .controller('CompileController', CompileController)
    .controller('InterpolateController', InterpolateController)
    .controller('ParseController', ParseController);

  /**
   * On Run
   */
  function howItWorks($rootScope) {
    $rootScope.title = 'How it works';
    $rootScope.modelOptions = {
      debounce: 1000
    };
  }


  /**
   * Compile Controller
   */
  function CompileController($scope, $compile, $element) {
    var $ctrl = this,
      compiledNodes = [];

    $scope.content = '<p>{{ title || \'undefined\' }}</p>';

    $ctrl.compile = function(content, isolate) {
      var wrapper = $('<div class="row"></div>').html(content),
        linkFn = $compile(wrapper),
        compiled = linkFn($scope.$new(isolate));
      $element.append(compiled);
      compiledNodes.push(compiled);
    };

    $ctrl.clear = function() {
      for (var i in compiledNodes) {
        compiledNodes[i].remove();
      }
    };
  }

  /**
   * Parse Controller
   */
  function ParseController($scope, $parse) {
    $scope.parseObj = {
      returnNumber: function(num) {
        return num;
      },

      // Set attribute on object
      set: function(path, value) {
        $parse(path).assign($scope.parseObj, value);
      },

      parse: {
        goes: {
          deep: true
        }
      },
      deep: {
        dive: {
          value: 'bottom'
        }
      },
      my: {
        name: 'Michael Bonds'
      }
    };

    $scope.$watch('parseText', function(text) {
      $scope.output = $parse($scope.parseText)($scope.parseObj);
    });
  }

  /**
   * Interpolate Controller
   */
  function InterpolateController($scope, $interpolate, $element) {
    var $ctrl = this;

    $scope.interpolateText = '{{ my.name.is }}';

    $scope.model = {
      scopeName: 'Interpolate Controller Scope',
      my: {
        name: {
          is: 'Michael'
        }
      },
      myName: function() {
        return $scope.model.my.name.is;
      }
    };

    $ctrl.interpolate = function(interpolatedText) {
      $scope.output = $interpolate(interpolatedText)($scope.model);
    }
  }

  /**
   * Digest Cycle Count Directive
   */
  function DigestCycleCountDirective() {
    return {
      restrict: 'E',
      template: '<div></div>',
      scope: true,
      link: function(scope, el) {
        var count = 0;

        scope.$watch(function() {
          count++;
          el.html(count);
        });
      }
    };
  }
}());
