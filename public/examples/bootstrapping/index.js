(function() {
  'use strict';

  angular.module('BootstrappingExample', [])
    .directive('myDirective', myDirective)
    .controller('MainController', MainController);

  function MainController($scope) {
    $scope.title = 'Bootstrapping Example';
  }

  /**
   * My Directive
   */
  function myDirective() {
    return {
      restrict: 'EA',
      scope: true,
      transclude: true,
      template: '<div><ng-transclude></ng-transclude></div>',
      controller: function($element) {
        console.log($element[0], 'My Directive Controller');
        // Ran for every element
        // Runs before pre-link(2)
      },
      compile: function(templateElement, templateAttrs) {
        console.log(templateElement[0], 'compile phase');
        // Runs first (1)
        // Ran once during compile phase
        // only do DOM transformations
        // no event listeners

        return {
          pre: function(scope, el, attrs) {
            console.log(el[0], 'pre-link-phase');
            // Ran for every element
            // No DOM transformations
            // Runs after controller-instantiation (3)
          },
          post: function(scope, el, attrs) {
            console.log(el[0], 'post-link-phase');
            // Ran once for every element
            // Register Listeners, configurable DOM transformations, etc..
            // Runs after all children pre-link and controller-instantiations (4)
          }
        };
      },
      link: function() {
        // same as this.compile().pre() ^^
      }
    }
  }
}());
