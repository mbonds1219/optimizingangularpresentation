(function() {
  'use strict';

  angular.module('BootstrappingExample', [])
    .directive('myDirective', myDirective)
    .controller('MainController', MainController);

  function MainController($scope) {
    $scope.title = 'Bootstrapping Example';
    $scope.repeater = (new Array(100)).fill(1);
  }

  /**
   * My Directive
   */
  function myDirective() {
    return {
      restrict: 'EA',
      scope: true,
      transclude: true,
      template: '<div></div>',
      compile: function(templateElement, templateAttrs) {

        return {
          pre: function(scope, el, attrs) {
            angular.element(el).attr(attrs.attribute, attrs.attributeValue);
          }
        };
      }
    };
  }
}());
