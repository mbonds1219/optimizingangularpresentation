(function() {
  'use strict';

  angular.module('NgIfvsNgShow', [])
    .run(NgIfvsNgShow)
    .directive('senator', SenatorDirective);

    function NgIfvsNgShow($rootScope, $http) {
      var $scope = $rootScope;

      $scope.showDemocrats = false;
      $scope.showRepublicans = true;
      $scope.ngIf = true;

      $scope.shouldShow = function(senator) {
        return (
          ($scope.showRepublicans && senator.party === 'Republican') ||
          ($scope.showDemocrats && senator.party === 'Democrat')
        );
      };

      $http.get('/data/senators.json')
        .then(function(response) {
          $scope.senators = response.data.objects;
        });
    }

    function SenatorDirective() {
      return {
        restrict: 'E',
        templateUrl: './senatorTemplate.html',
        scope: {
          senator: '=',
          includeStates: '='
        },
        compile: function(element) {
          return function SenatorPostLink(scope, el, attrs) {
            scope.changeParty = function(senator) {
              if(senator.party === 'Democrat') {
                senator.party = 'Republican';
              } else {
                senator.party = 'Democrat';
              }
            };
          };
        }
      };
    }

}());
