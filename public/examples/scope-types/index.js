(function() {
  "use strict";

  var ScopeTypes = angular.module('ScopeTypes', []);

  ScopeTypes
      // .directive('testCompile', TestCompileDirective)
      .directive('isolateScope', IsolateScope)
      .directive('childScope', ChildScope)
      .directive('sameScope', SameScope)
      .controller('MainController', MainController);

  // For Equality check in directives
  var mainControllerScope;

  /**
   * ScopeTypes Main Controller
   *
   * @param $scope
   * @param $rootScope
   * @constructor
   */
  function MainController($scope, $injector) {
    var $ctrl = this;

    $scope.numbers = [1, 2, 3];
    $scope.name = 'MainControllerScope';
    $ctrl.name = 'MainController';

    mainControllerScope = $scope;

    $ctrl.changeName = function($event, val) {
      $event.stopImmediatePropagation();
      this.name = val;
    };

    $scope.changeName = function(val) {
      $scope.name = val;
    };
  }

  /**
   * Creates Isolate Scope
   *
   * @returns {*}
   * @constructor
   */
  function IsolateScope() {
    return {
      restrict: 'E',
      templateUrl: 'scopes.html',
      scope: {
        test: '@'
      },
      controller: ScopesController
    };
  }

  /**
   * Creates Child Scope
   *
   * @returns {{restrict: string, template, bindToController: {test: string}, controllerAs: string}}
   * @constructor
   */
  function ChildScope() {
    return {
      restrict: 'E',
      templateUrl: 'scopes.html',
      scope: true,
      controller: ScopesController
    };
  }

  /**
   * Shares Scope
   *
   * @returns {{restrict: string, templateUrl: string, scope: boolean, bindToController: {test: string}, controller: controller}}
   * @constructor
   */
  function SameScope() {
    return {
      restrict: 'E',
      templateUrl: 'scopes.html',
      scope: false, // Default
      bindToController: {
        test: '@'
      },
      controller: function($scope, $rootScope) {
        console.groupCollapsed('SameScope');
        console.log('$rootScope === $scope.$parent', $rootScope === $scope.$parent); // true
        console.log('$scope === mainControllerScope', $scope === mainControllerScope); // true
        console.groupEnd('SameScope');
      }
    };
  }

  /**
   * Scopes Controller
   *
   * @param $scope
   * @param $parse
   * @param $attrs
   * @constructor
   */
  function ScopesController($scope, $element, $attrs, $rootScope) {
    console.groupCollapsed($element[0].nodeName);
    console.log('$rootScope === $scope.$parent', $rootScope === $scope.$parent); // false
    console.log('$scope === mainControllerScope', $scope === mainControllerScope);
    console.groupEnd($element[0].nodeName);

    $scope.attribute = $attrs.test;

    $scope.$watch($attrs.test, function(val) {
      $scope.value = val || 'undefined';
    });

    $scope.$parent.$watch($attrs.test, function(val) {
      $scope.parentValue = val || 'undefined';
    });
  }

})();
