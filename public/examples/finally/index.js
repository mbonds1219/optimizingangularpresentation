(function() {
  "use strict";

  angular.module('Finally', [])
      .run(Finally);

  /**
   *
   * @param $scope
   * @param $http
   * @constructor
   */
  function Finally($scope, $http) {
    $http.get('/data/airlines.json')
        .then(function (response) {
          $scope.data = response;
        });
  }

})();