(function() {
  'use strict';

  angular.module('FlightStatistics', [])
    .controller('MainCtrl', MainController);


  MainController.$inject = ['$scope', '$http'];

  function MainController($scope, $http) {
    var stats = $http.get('/airlines.json');

    stats.then(function(response) {
      console.log(response);
    });

    stats.catch(function (err) {
      console.log(err);
    });

    $scope.stats = stats;

  }


})();
