
/**
 * Configuration
 */
(function() {
  'use strict';

  angular.module('main', [])
    .config(configure);

  function configure($compileProvider) {
    $compileProvider.debugMode = !!window.DEBUG;
  };
}());

/**
 * HttpFallback Service
 */
(function() {
  'use strict';

  angular.module('http-fallback', [])
    .service('httpFallback', HttpFallback);

  function HttpFallback($http, $q) {
    this.get = function(url, fallback) {
      var deferred = $q.defer();
      var promise = $http.get(url);

      promise.then(deferred.resolve, function(err) {
        $http.get(fallback).then(deferred.resolve, deferred.reject);
      });

      return deferred.promise;
    };
  }
}());
