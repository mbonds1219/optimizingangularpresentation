
/**
 * ISS Data Provider
 */
(function() {
  'use strict';
  angular.module('iss', ['http-fallback'])
    .provider('iss', IssProvider);

  function IssProvider() {
    console.log('Provider Registered', this.constructor.name);
    this.$get = IssService;
  }

  function IssService(httpFallback) {
    console.log('instantiated', this.constructor.name);
    this.getLocation = function() {
      return httpFallback.get('http://api.open-notify.org/iss-now.json', '/data/iss.json');
    };
  }

  IssService.prototype.getLocation = function() {
  };
}());

/**
 * Reddit Data Provider
 */
(function() {
  'use strict';
  angular.module('reddit', ['http-fallback'])
    .service('reddit', Reddit);

  /**
   * Connection to reddit
   */
  function Reddit(httpFallback) {
    console.log('instantiated', this.constructor.name);

    this.baseUri = 'https://www.reddit.com/r/';

    this.programmerHumor = function() {
      return this.getSubreddit('programmerhumor');
    };

    this.getSubreddit = function(subreddit) {
      return httpFallback.get(this.baseUri + subreddit + '.json', '/data/programmerHumor');
    };
  }

}());
